#!/usr/bin/env python

from __future__ import print_function
from __future__ import division

import logging
import os
import shutil
import socket
import subprocess
import threading
import zipfile

TCP_IP = '192.168.1.101'
TCP_PORT = 9999

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S')

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((TCP_IP, TCP_PORT))
server.listen(2)

print('\nPS2cc (PS2 cloud compiler) - listening on {}:{}'.format(TCP_IP, TCP_PORT))


def send_file(connection, compiled, file_path):
    connection.send(compiled)
    if 'RECIEVED' in connection.recv(8):
        connection.send(str(os.path.getsize(file_path)))
        if 'RECIEVED' in connection.recv(8):
            f = open(file_path, 'rb')
            chunk = f.read(1024)
            while chunk:
                connection.send(chunk)
                chunk = f.read(1024)
            f.close()


def handle_connection(connection, address):
    file_size = connection.recv(50)
    connection.send('RECIEVED')

    elf_name = connection.recv(256)
    buffer = ''
    connection.send('RECIEVED')

    while len(buffer) < int(file_size):
        buffer += connection.recv(1024)

    zip_path = os.getcwd() + os.sep + 'compile.zip'
    zip_output_file = open(zip_path, 'wb')
    zip_output_file.write(buffer)
    zip_output_file.close()

    zip_ref = zipfile.ZipFile(zip_path, 'r')
    zip_ref.extractall(os.getcwd() + os.sep + 'compile')
    zip_ref.close()

    os.remove(zip_path)
    os.chdir(os.getcwd() + os.sep + 'compile')

    make_log_path = os.getcwd() + os.sep + 'make_log.txt'
    make_log_file = open(make_log_path, 'w+')

    proc = subprocess.Popen(["make"], stderr=make_log_file, stdout=make_log_file)
    stdout, stderr = proc.communicate()

    make_log_file.close()

    elf_path = os.getcwd() + os.sep + elf_name

    if os.path.isfile(elf_path):
        send_file(connection, '1', elf_path)
    else:
        send_file(connection, '0', make_log_path)

    os.chdir(os.getcwd() + os.sep + '..' + os.sep)
    shutil.rmtree(os.getcwd() + os.sep + 'compile')

    logging.info('Closing connection with client {}:{}'.format(address[0], address[1]))
    connection.close()


if __name__ == '__main__':
    while True:
        try:
            connection, address = server.accept()
            logging.info('Accepted connection from {}:{}'.format(address[0], address[1]))
            th = threading.Thread(target=handle_connection, args=(connection, address,))
            th.start()
        except:
            print('\n')
            break
