#!/usr/bin/env python

from __future__ import print_function
from __future__ import division

import logging
import os
import shutil
import socket
import sys

SERVER_IP = '192.168.1.101'
SERVER_PORT = 9999

if __name__ == '__main__':
    print('\nPS2cc (PS2 cloud compiler)')
    
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S')
    if len(sys.argv) == 3:
        selected_folder = sys.argv[1]
        if os.path.isdir(selected_folder):
            zip_path = os.path.dirname(os.path.realpath(sys.argv[0])) + os.sep + 'zipped'
            shutil.make_archive(zip_path, 'zip', selected_folder)
            zip_path += '.zip'

            try:
                server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                server.connect((SERVER_IP, SERVER_PORT))
                server.send(str(os.path.getsize(zip_path)))

                if 'RECIEVED' in server.recv(8):
                    elf_name = str(sys.argv[2])
                    if '.elf' not in elf_name:
                        elf_name += '.elf'
                    server.send(elf_name)

                    if 'RECIEVED' in server.recv(8):
                        zip_file = open(zip_path, 'rb')
                        chunk = zip_file.read(1024)
                        while chunk:
                            server.send(chunk)
                            chunk = zip_file.read(1024)
                        zip_file.close()
                        logging.info('Data uploaded.\n\tPlease wait for the server to compile and return your elf file.')
                    else:
                        logging.warning('The server is not responding at the moment!')

                    os.remove(zip_path)
                    elf_compiled = server.recv(1)
                    server.send('RECIEVED')

                    if elf_compiled == '1':
                        logging.info('The elf file was successfully compiled.')
                        file_path = os.path.dirname(os.path.realpath(sys.argv[0])) + os.sep + elf_name
                    else:
                        logging.error('The elf file failed to compile.')
                        file_path = os.path.dirname(os.path.realpath(sys.argv[0])) + os.sep + 'make_log.txt'

                    file_size = server.recv(50)
                    logging.info('Recieving file of %s bits...' % file_size)

                    buffer = ''
                    server.send('RECIEVED')

                    while len(buffer) < int(file_size):
                        buffer += server.recv(1024)

                    out_file = open(file_path, 'wb')
                    out_file.write(buffer)
                    out_file.close()
            except:
                os.remove(zip_path)
                print('\nUnable to establish a connection with the server!\n')
        else:
            print('\nThe path that you have entered does not point to a valid directory!\n')
    else:
        print('\nScript requires the directory path and output elf name as a parameters:\n'
              + '  python ps2cc.py /home/pi/ps2dev/open-ps2-loader opl.elf\n')
